import { writeFile } from 'node:fs/promises';
import { resolve } from 'node:path';
import { generateSnippetsIndex } from './generate-snippets-index.js';

const path = resolve(import.meta.dirname, '../public/generated/snippets-index.js');
const content = await generateSnippetsIndex()
await writeFile(path, content, 'utf-8');
