import { readFileSync } from 'node:fs';
import { resolve } from 'node:path';

const mainContent = String(readFileSync(resolve(import.meta.dirname, '../public/main.js')))
		.split(/\r?\n/)
		.filter(line => !line.includes('// @disabled by middlewares/serve-dev-sw.js'))
		.join('\n');

export async function serveDevSw(ctx, next) {
	if (ctx.method !== "GET" || ctx.url !== '/main.js') {
		return next();
	}
	ctx.set('content-type', 'application/javascript; charset=utf-8');
	ctx.body = mainContent;
}
