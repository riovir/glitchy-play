import { resolve } from 'node:path';
import { access, readdir, stat } from 'node:fs/promises';
import { constants } from 'node:fs';

export async function generateSnippetsIndex() {
	const snippetPath = resolve(import.meta.dirname, '../public/snippets');
	const toSnippetPath = toSnippetPathIn(snippetPath);
	const files = await readdir(snippetPath);
	const maybeSnippets = await Promise.all(files.map(toSnippetPath));

	const snippetFileUrls = maybeSnippets
		.filter(Boolean)
		.sort()
		.map(path => `snippets/${path}`)
		.map(url => `\t'${url}',`);

	return `
export const snippets = [
${snippetFileUrls.join('\n')}
];
`;
}

function toSnippetPathIn(dir) {
	return async function toSnippetPath(path) {
		const absolutePath = resolve(dir, path);
		const isDirectory = (await stat(absolutePath)).isDirectory();
		if (!isDirectory && path.toLowerCase().endsWith('.js')) {
			return path;
		}
		const indexPath = resolve(dir, path, 'index.js');
		if (await exists(indexPath)) {
			return `${path}/index.js`;
		};
		return null;
	}
}

function exists(path) {
	return access(path, constants.F_OK)
		.then(() => true)
		.catch(() => false);
}
