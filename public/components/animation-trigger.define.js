const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: inline-block;
	}

	#trigger {
		border: none;
		cursor: pointer;
		background: transparent;
		transition: filter 0.2s ease-in-out, transform 0.2s ease-in-out;
		-webkit-tap-highlight-color: transparent;
	}
	#trigger:hover {
		filter: brightness(110%);
	}
</style>
<button id="trigger">
	<slot></slot>
</button>
`;

class AnimationTrigger extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._targets = [];
		this._targetFunction = 'animateSprite';

		this._triggerElement = shadowRoot.querySelector('#trigger');
		this.animateTargets = this.animateTargets.bind(this);
		this._handleTouchMove = this._handleTouchMove.bind(this);
		this._animationBusy = false;

		shadowRoot.addEventListener('slotchange', () => {
			const canAnimate = element => typeof element[this.targetFunction] === 'function';
			this._targets = filterChildrenDeep(canAnimate, this);
		});

		this._triggerElement.addEventListener('click', this.animateTargets);
	}

	connectedCallback() {
		if (!this.iterations) {
			this.iterations = 1;
		}
		document.addEventListener('touchstart', this._handleTouchMove);
		document.addEventListener('touchmove', this._handleTouchMove);
	}

	disconnectedCallback() {
		document.removeEventListener('touchstart', this._handleTouchMove);
		document.removeEventListener('touchmove', this._handleTouchMove);
	}

	get targets() {
		return this._targets;
	}

	set targetFunction(value) {
		this._targetFunction = value;
		this.setAttribute('target-function', value);
	}

	get targetFunction() {
		return this._targetFunction;
	}

	set iterations(value) {
		this.setAttribute('iterations', value);
	}

	get iterations() {
		if (!this.hasAttribute('iterations')) { return 1; }
		return Math.floor(this.getAttribute('iterations'));
	}

	_handleTouchMove({ touches }) {
		if (find(touchesAny(this._targets), touches)) {
			this.animateTargets();
		}
	}

	animateTargets() {
		if (this._animationBusy) { return; }
		this._animationBusy = true;
		const endAnimation = () => {
			this._animationBusy = false;
			this.dispatchEvent(new Event('animation-finished'));
		};
		return Promise.all(this._targets.map(target => {
			const animate = target[this._targetFunction];
			const animation = animate({ iterations: this.iterations, fill: 'both' });
			return animation.finished;
		}))
			.then(endAnimation)
			.catch(endAnimation);
	}
}

customElements.define('animation-trigger', AnimationTrigger);

function touchesAny(targets) {
	return ({ pageX, pageY }) => {
		const touchTarget = document.elementFromPoint(pageX, pageY);
		return targets.includes(touchTarget);
	};
}

function find(predicate, arrayLike) {
	for (let i = 0; i < arrayLike.length; i++) {
		const element = arrayLike[i];
		if (predicate(element)) {
			return element;
		}
	}
}

function filterChildrenDeep(predicate, element) {
	if (!element?.children?.length) { return []; }
	const children = [...element.children].filter(predicate);
	const childrenDeep = children.flatMap(child => filterChildrenDeep(predicate, child))
	return [...children, ...childrenDeep];
}
