import './puzzle-piece.js';
import { useUpdate } from './use-update.js';
import { preload } from './orientable-image.js';
import { cutoutIdOf, JigsawCutout, toSvg } from './jigsaw-cutout.js';

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		position: relative;
		box-sizing: border-box;
		display: grid;
		grid-template: 'host' 1fr / 1fr;
		aspect-ratio: var(--puzzle-board-aspect-ratio, 1);
		width: 100%;
		height: 100%;
	}
	::slotted(*) {
		grid-area: container;
	}

	#content {
		position: absolute;
		display: grid;
		grid-area: host;
		grid-template: 'container';
		width: 425px;
		height: 425px;
	}

	#canvas {
		grid-area: host;
		max-height: 100%;
		aspect-ratio: var(--puzzle-board-aspect-ratio, 1);
		overflow: hidden;
		box-sizing: border-box;
		position: relative;
	}
</style>
<div id="canvas"></div>
<div id="content">
	<slot></slot>
</div>
<slot name="cutout"></slot>
`;

class PuzzleBoard extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._src = '';
		this._canvasElement = shadowRoot.querySelector('#canvas');
		this._contentElement = shadowRoot.querySelector('#content');
		this._cutoutDefs = shadowRoot.querySelector('#cutout-defs');
		this._updateContentSize = this._updateContentSize.bind(this);
		this._sizeObserver = new ResizeObserver(this._updateContentSize);

		this.update = this.update.bind(this);

		const { complete: _updateComplete, requestUpdate } = useUpdate(this);
		Object.assign(this, { _updateComplete, requestUpdate });

		document.body.addEventListener('fullscreenchange', this._updateContentSize);
	}

	connectedCallback() {
		this.src = this.src || this.getAttribute('src');
		this._sizeObserver.observe(this);
		if (!this.src) { return; }
		this.requestUpdate();
	}

	disconnectedCallback() {
		this._sizeObserver.disconnect();
	}

	set orientation(value) {
		this.setAttribute('orientation', value);
		if (!this.src || !this.parentElement) { return; }
		this.requestUpdate('orientation');
	}
	get orientation() {
		return this.getAttribute('orientation');
	}

	get piecesX() {
		return parseInt(this.getAttribute('pieces-x')) || 5;
	}
	set piecesX(value) {
		if (value === this.piecesX) { return; }
		this.setAttribute('pieces-x', value);
		this.requestUpdate('piecesX');
	}

	get piecesY() {
		return parseInt(this.getAttribute('pieces-y')) || 5;
	}
	set piecesY(value) {
		if (value === this.piecesY) { return; }
		this.setAttribute('pieces-y', value);
		this.requestUpdate('piecesY');
	}

	get seed() {
		return this.getAttribute('seed') || 'apple';
	}
	set seed(value) {
		if (value === this.seed) { return; }
		this.setAttribute('seed', value);
		this.requestUpdate('seed');
	}

	get src() {
		return this._src;
	}
	set src(value) {
		this._src = value;
		this.dispatchEvent(new Event('src-changed', { bubbles: true, composed: true }));
		this.requestUpdate('src');
	}

	async update(changedProps) {
		if (!changedProps.size || hasAny(['piecesX', 'piecesY', 'seed', 'orientation'], changedProps)) {
			this._updatePieces();
		}
		if (!this._src) { return; }
		const { naturalWidth, naturalHeight } = await preload(this);
		this.style.setProperty('--puzzle-board-aspect-ratio', `${naturalWidth} / ${naturalHeight}`);
		await wait(50);
		for (const piece of this.querySelectorAll('puzzle-piece')) {
			piece.src = this.src;
		}
		this._updateContentSize();
		this.dispatchEvent(new Event('ready'));
	}

	_updatePieces() {
		this.innerHTML = toSvg(JigsawCutout(this));
		range(this.piecesX * this.piecesY).forEach(index => {
			const x = index % this.piecesX;
			const y = Math.floor(index / this.piecesX);
			this.appendChild(MoveablePiece({
				index, x, y,
				id: `p${index + 1}`,
				cutoutId: cutoutIdOf(index),
				src: this.src,
				orientation: this.orientation,
			}));
		});
	}

	get canvasWidth() { return this._canvasElement.clientWidth; }
	get canvasHeight() { return this._canvasElement.clientHeight; }

	_updateContentSize() {
		const { clientWidth: canvasWidth, clientHeight: canvasHeight } = this._canvasElement;
		this._contentElement.style.width = `${canvasWidth}px`;
		this._contentElement.style.height = `${canvasHeight}px`;
		const { piecesX, piecesY } = this;
		const { clientWidth: parentWidth, clientHeight: parentHeight } = this.parentElement;
		for (const moveable of this.querySelectorAll('moveable-group')) {
			const { props, dataset } = calculatePiece({
				x: parseInt(moveable.dataset.x),
				y: parseInt(moveable.dataset.y),
				piecesX, piecesY,
				canvasWidth, canvasHeight,
				parentWidth, parentHeight,
			});
			Object.assign(moveable, props);
			Object.assign(moveable.dataset, dataset);
		}
	}
}

function MoveablePiece({ id, cutoutId, orientation, src, index, x, y }) {
	const moveableGroup = document.createElement('moveable-group');
	Object.assign(moveableGroup, { id });
	Object.assign(moveableGroup.dataset, { index, x, y });
	const piece = document.createElement('puzzle-piece');
	Object.assign(piece, { cutoutId, orientation, src });
	moveableGroup.appendChild(piece);
	return moveableGroup;
}

function range(length) {
	return [...Array(length).keys()];
}

function calculatePiece({ x, y, piecesX, piecesY, canvasWidth, canvasHeight, parentWidth, parentHeight }) {
	const pieceWidth = canvasWidth / piecesX;
	const pieceHeight = canvasHeight / piecesY;
	const offsetXMin = -1 * Math.floor(x * pieceWidth);
	const offsetXMax = parentWidth - (x + 1) * pieceWidth;
	const offsetYMin = -1 * Math.floor(y * pieceHeight);
	const offsetYMax = parentHeight - (y + 1) * pieceHeight;
	const props = { offsetXMin, offsetXMax, offsetYMin, offsetYMax };

	const toFixed = number => Number.parseFloat(number).toFixed(1);
	const centerX = toFixed((x * pieceWidth) + pieceWidth / 2);
	const centerY = toFixed((y * pieceHeight) + pieceHeight / 2);
	const dataset = { centerOrigin: `${centerX}px ${centerY}px`, x, y, pieceWidth, pieceHeight };

	return { props, dataset };
}

function wait(msec) {
	return new Promise(resolve => setTimeout(resolve, msec));
}

customElements.define('puzzle-board', PuzzleBoard);

function hasAny(elements, map) {
	return elements.find(element => map.has(element));
}
