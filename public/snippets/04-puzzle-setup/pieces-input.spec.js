import { calculatePieces } from './pieces-input.js';

test('pieces-input / calculatePieces returns 1x1 by default', () => {
	const pieces = calculatePieces({});
	expect(pieces).toBeSame({ piecesX: 1, piecesY: 1 });
});

test('pieces-input / calculatePieces limits piecesX to max', () => {
	const pieces = calculatePieces({ piecesXMax: 2, piecesPreferred: 99, aspectRatio: 1 });
	expect(pieces).toBeSame({ piecesX: 2, piecesY: 2 });
});

test('pieces-input / calculatePieces limits piecesY to max', () => {
	const pieces = calculatePieces({ piecesYMax: 2, piecesPreferred: 99, aspectRatio: 1 });
	expect(pieces).toBeSame({ piecesX: 2, piecesY: 2 });
});

test('pieces-input / calculatePieces picks best shape match when reaching preference', () => {
	expect(calculatePieces({ piecesPreferred: 5, aspectRatio: 1 })).toBeSame({ piecesX: 2, piecesY: 2 });
	expect(calculatePieces({ piecesPreferred: 6, aspectRatio: 1 })).toBeSame({ piecesX: 3, piecesY: 3 });
	expect(calculatePieces({ piecesPreferred: 9, aspectRatio: 1 })).toBeSame({ piecesX: 3, piecesY: 3 });
	expect(calculatePieces({ piecesPreferred: 100, aspectRatio: 4 / 3 })).toBeSame({ piecesX: 12, piecesY: 9 });
});

test('pieces-input / calculatePieces limits results to max at the expense of preferred pieces', () => {
	const calculate = options => calculatePieces({ ...options, piecesXMax: 7 })
	expect(calculate({ piecesPreferred: 100, aspectRatio: 4 / 3 })).toBeSame({ piecesX: 7, piecesY: 5 });
	expect(calculate({ piecesPreferred: 100, aspectRatio: 3 / 4 })).toBeSame({ piecesX: 7, piecesY: 9 });
});
