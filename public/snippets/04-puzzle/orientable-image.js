const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		aspect-ratio: var(--aspect-ratio, 1);
		min-height: 32px;
		max-width: 100%;
		max-height: 100%;
		position: relative;
		pointer-events: none;
	}
	#image {
		position: absolute;
		inset: 0;
		width: 100%;
	}
	#image.is-rotated {
		width: var(--height, 32px);
		height: var(--width);
		transform-origin: top left;
		transform: rotate(90deg) translateY(calc(-1 * var(--width)));
	}
</style>
<img id="image" rel="preload">
`;

class OrientableImage extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this.updated = Promise.resolve('blank');

		this._updateOrientation = this._updateOrientation.bind(this);
		this._updateSrc = this._updateSrc.bind(this);
		this._updateSize = this._updateSize.bind(this);

		this._imageElement = shadowRoot.querySelector('#image');

		this._imageElement.addEventListener('load', () => {
			this.dispatchEvent(new Event('load', { bubbles: true, composed: true }));
		});
		this._observer = new ResizeObserver(this._updateSize);
	}

	connectedCallback() {
		upgradeProperty(this, 'src');
		upgradeProperty(this, 'orientation');

		if (this.hasAttribute('src')) {
			this.src = this.getAttribute('src');
		}
		if (this.hasAttribute('orientation')) {
			this.orientation = this.getAttribute('orientation');
		}
		this._observer.observe(this);
	}

	get orientation() {
		return this.getAttribute('orientation');
	}
	set orientation(value) {
		if (value) { this.setAttribute('orientation', value); }
		else { this.removeAttribute('orientation'); }
		this._updateOrientation();
	}

	get isOrientationFlipped() {
		const { naturalHeight, naturalWidth } = this._imageElement;
		return (this.orientation === 'portrait' && naturalHeight < naturalWidth) ||
				(this.orientation === 'landscape' && naturalWidth < naturalHeight);
	}

	get src() {
		return this._imageElement.src;
	}
	set src(value) {
		this.setAttribute('src', value);
		this._updateSrc(value).then(this._updateOrientation);
	}

	async _updateSrc(src) {
		if (!src) { return; }
		this.updated = new Promise(resolve => {
			this._imageElement.onload = resolve;
		});
		this._imageElement.src = src;
		return this.updated;
	}

	get naturalHeight() {
		return this.isOrientationFlipped ?
				this._imageElement.naturalWidth :
				this._imageElement.naturalHeight;
	};
	get naturalWidth() {
		return this.isOrientationFlipped ?
				this._imageElement.naturalHeight :
				this._imageElement.naturalWidth;
	};

	_updateOrientation() {
		this._imageElement.className = this.isOrientationFlipped ? 'is-rotated' : '';
		const { naturalWidth, naturalHeight } = this._imageElement;
		const aspectRatio = this.isOrientationFlipped ?
				`${naturalHeight} / ${naturalWidth}` :
				`${naturalWidth} / ${naturalHeight}`;
		this.style.setProperty('--aspect-ratio', aspectRatio);
	}

	_updateSize() {
		this._imageElement.style.setProperty('--width', `${this.clientWidth}px`);
		this._imageElement.style.setProperty('--height', `${this.clientHeight}px`);
	}
}

export async function preload({ src, orientation }) {
	const image = document.createElement('orientable-image');
	Object.assign(image, { orientation, src });
	await image.updated;
	return image;
}

function upgradeProperty(host, prop) {
	if (Object.hasOwn(host, prop)) {
	  const value = host[prop];
	  delete host[prop];
	  host[prop] = value;
	}
  }

customElements.define('orientable-image', OrientableImage);
