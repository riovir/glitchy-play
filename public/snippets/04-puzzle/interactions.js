import { complement, filter, partition, prop, shuffle, without } from './utils.js';
import { maxSizeOf } from './jigsaw-cutout.js';
import { assocDrawerIndex, drawerIndexOf, isInDrawer } from './piece-data.js';

export function Interactions({ board, pieces, random, reflect, pieceDrawer, playClickSound, snapRangePx = 16 }) {
	const drawerPieces = {};
	const connections = {};

	const canSnap = arePointsWithin({ range: snapRangePx });
	const boundingPieceRect = boundingPieceRectOn({ board });
	const distanceToVisible = distanceToVisibleOn({ board, boundingPieceRect });
	const connectedPiecesOf = connectedPiecesOn({ pieces, canSnap });
	const snapsIntoDrawer = snapsIntoDrawerOn({ board, pieceDrawer, connectedPiecesOf });

	const scrambleBoard = async ({ rotate }) => {
		await new Promise(resolve => setTimeout(resolve, 150));
		const randomBetween = (min, max) => Math.floor(random() * (max - min) + min);
		for (const target of pieces) {
			if (rotate) {
				const rotation = randomBetween(0, 4) * 90;
				const { centerOrigin: around } = target.dataset;
				await target.rotate(rotation, { around, animate: false });
			}
		}
		shuffle(pieces).forEach(piece => drawerAdd(piece));
	}

	const movePiece = ({ target }) => {
		const { id, offsetX, offsetY } = target;
		for (const piece of connections[id] ?? []) {
			Object.assign(piece, { offsetX, offsetY });
		}
	};
	const onMove = ({ target }) => {
		movePiece({ target });
		const pieces = [target, ...connections[target.id] ?? []];
		const { x, y } = minDistanceToVisible({ pieces, distanceToVisible });
		if (x === 0 && y === 0) { return; }
		for (const piece of pieces) {
			piece.offsetX += x;
			piece.offsetY += y;
		}
	};
	const afterMoved = ({ target, sound }) => {
		const connectedPieces = connectedPiecesOf(target);

		const pieceSolved = isSolved({ canSnap, piece: target });
		if (pieceSolved) {
			solvePiece(target);
			connectedPieces.forEach(solvePiece);
			reflect({ target });
		}

		connectedPieces.forEach(piece => reflect({ target: piece }));
		const pieceConnected = findNewConnectedPiece({ connections, connectedPieces: connectedPieces });
		if (sound && (pieceSolved || pieceConnected)) { playClickSound(); }

		if (!pieceConnected) { return; }
		const isDifferentFrom = piece => candidate => piece.id !== candidate.id;
		const { offsetX, offsetY } = pieceConnected;
		connectedPieces.forEach(target => {
			Object.assign(target, { offsetX, offsetY })
			reflect({ target });
		});
		for (const piece of connectedPieces) {
			connections[piece.id] = connectedPieces.filter(isDifferentFrom(piece));
		}
	}
	const onMoveEnd = async ({ target, sound = true }) => {
		movePiece({ target });
		const updateDrawer = snapsIntoDrawer(target) ? drawerAdd : drawerRemove;
		updateDrawer(target);
		afterMoved({ target, sound });
		if (isSolvedBoard({ pieces })) {
			await playSolvedEffect({ board, pieceDrawer });
		}
	}

	let ignore = false;
	const onTapRotate = async ({ target }) => {
		if (ignore) { return; }
		ignore = true;
		const { centerOrigin: around } = target.dataset;
		const group = [target, ...connections[target.id] ?? []]
		await Promise.all(group.map(async piece => {
			await piece.rotate(90, { around });
			reflect({ target: piece });
		}));
		if (!isInDrawer(target)) {
			afterMoved({ target, sound: true });
		}
		ignore = false;
	};


	const updateDrawer = updateDrawerOf({ board, pieceDrawer, drawerPieces });
	const drawerAdd = (piece, { index } = {}) => {
		if (!isInDrawer(piece)) {
			if (!pieceDrawer.active) { pieceDrawer.active = true; };
			const _index = index ?? Object.keys(drawerPieces).length;
			drawerPieces[piece.id] = assocDrawerIndex(_index, piece);
		}
		updateDrawer();
	};
	const drawerRemove = piece => {
		if (!isInDrawer(piece)) { return; }
		assocDrawerIndex(null, drawerPieces[piece.id]);
		delete drawerPieces[piece.id];
		Object.values(drawerPieces)
			.sort(ascendBy(drawerIndexOf))
			.forEach((piece, index) => assocDrawerIndex(index, piece));
		setTimeout(updateDrawer, 0);
	};

	let moveAnimations = Promise.resolve();
	const onDrawerMove = async ({ detail: { deltaX = 0, deltaY = 0, duration = 0 } = {} }) => {
		await moveAnimations;
		const movePiece = piece => piece.move({
			duration,
			offsetX: piece.offsetX + deltaX,
			offsetY: piece.offsetY + deltaY,
		});
		const piecesInside = Object.values(drawerPieces);
		moveAnimations = Promise.all([...piecesInside.map(movePiece), wait(duration)]);
		await moveAnimations;
		const piecesOutside = filter(complement(isInDrawer), pieces);
		const newPieces = filter(snapsIntoDrawer, piecesOutside);
		newPieces.forEach(drawerAdd);
		updateDrawer();
		piecesInside.forEach(target => reflect({ target }));
	};
	return { drawerAdd, scrambleBoard, onMove, onMoveEnd, onTapRotate, onDrawerMove };
}

function updateDrawerOf({ board, pieceDrawer, drawerPieces, gap = 16 }) {
	const boundingPieceRect = boundingPieceRectOn({ board });
	const sizeOf = piece => {
		const width = Number(piece.dataset.pieceWidth);
		const height = Number(piece.dataset.pieceHeight);
		return Math.max(width, height);
	}
	const add = (a, b) => a + b;
	return () => {
		const pieces = Object.values(drawerPieces).sort(ascendBy(drawerIndexOf));
		if (!pieces.length) { return; }
		const sizeNeeded = pieces.map(sizeOf)
				.map(maxSizeOf)
				.reduce(add, 0);
		pieceDrawer.contentLength = sizeNeeded;
		const { scrollLeft, scrollTop, isHorizontal } = pieceDrawer;
		const { left: drawerLeft, top: drawerTop } = boundingRelativeRectOf(pieceDrawer);
		const scaleRect = scaleRectBy(maxSizeOf(1));

		let spaceBefore = 0;
		pieces.forEach(piece => {
			const { left, right, top, bottom, width, height } = scaleRect(boundingPieceRect(piece));
			const diffToCenter = Math.max(isHorizontal ? width - height : height - width, 0) / 2;
			const offsets = isHorizontal ?
				{
					offsetX: piece.offsetX + drawerLeft - left + spaceBefore - scrollLeft,
					offsetY: piece.offsetY + drawerTop - bottom + height + gap + diffToCenter,
				} : {
					offsetX: piece.offsetX + drawerLeft - right + width + gap + diffToCenter,
					offsetY: piece.offsetY + drawerTop - top + spaceBefore - scrollTop,
				};
			spaceBefore += Math.max(width, height);
			Object.assign(piece, offsets);
		});
	};
}

function boundingRelativeRectOf(element) {
	const { left: parentLeft, top: parentTop } = element.parentElement.getBoundingClientRect();
	const { left, right, top, bottom, width, height } = element.getBoundingClientRect();
	return {
		width, height,
		left: left - parentLeft,
		right: right - parentLeft,
		top: top - parentTop,
		bottom: bottom - parentTop,
	};
}

function scaleRectBy(multiplier) {
	return ({ left, right, top, bottom, width, height }) => {
		const widthScaled = width * multiplier;
		const widthDiff = widthScaled - width;
		const heightScaled = height * multiplier;
		const heightDiff = heightScaled - height;
		return {
			left: left - widthDiff / 2,
			right: right + widthDiff / 2,
			top: top - heightDiff / 2,
			bottom: bottom + heightDiff / 2,
			width: widthScaled,
			height: heightScaled,
		};
	};
}

function snapsIntoDrawerOn({ board, pieceDrawer, connectedPiecesOf }) {
	const boundingPieceRect = boundingPieceRectOn({ board });
	const isConnected = piece => 1 < connectedPiecesOf(piece).length;
	return piece => {
		if (piece.hasAttribute('disabled') || isConnected(piece)) { return false; }
		const { left: pieceLeft, top: pieceTop, width: pieceWidth, height: pieceHeight } = boundingPieceRect(piece);
		const centerX = pieceLeft + pieceWidth / 2;
		const centerY = pieceTop + pieceHeight / 2;
		const { left, right, top, bottom } = boundingRelativeRectOf(pieceDrawer);
		return left < centerX && centerX < right && top < centerY && centerY < bottom;
	};
}

function connectedPiecesOn({ pieces, canSnap = arePointsWithin({ range: 0 }) }) {
	return piece => {
		const activePieces = filter(complement(isInDrawer), pieces);
		const alignedPieces = filter(matchesWith({ canSnap, piece }), activePieces);
		return filterConnectedTo({ target: piece, pieces: alignedPieces });
	};
}

function minDistanceToVisible({ pieces, distanceToVisible }) {
	const [{ x = 0, y = 0, distance = 0 } = {}] = pieces
			.map(piece => ({ piece, ...distanceToVisible(piece) }))
			.sort(ascendBy(prop('distance')));
	return { x, y, distance };
}

function distanceToVisibleOn({ board, boundingPieceRect }) {
	return piece => {
		const { clientWidth, clientHeight } = board;
		const { left, right, top, bottom } = boundingPieceRect(piece);
		const xDistance = distanceToVisibleX({ pieceLeft: left, pieceRight: right, clientWidth });
		const yDistance = distanceToVisibleY({ pieceTop: top, pieceBottom: bottom, clientHeight });
		const distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
		return { x: xDistance, y: yDistance, distance };
	}
}

function boundingPieceRectOn({ board }) {
	return piece => {
		const { piecesX, piecesY } = board;
		const centerX = piecesX / 2 - 0.5;
		const centerY = piecesY / 2 - 0.5;
		const { offsetX, offsetY, rotation } = piece;
		const pieceX = parseInt(piece.dataset.x);
		const pieceY = parseInt(piece.dataset.y);
		const width = Number(piece.dataset.pieceWidth);
		const height = Number(piece.dataset.pieceHeight);
		const isOnSide = rotation / 90 % 2 === 1;
		const diffWidth = isOnSide ? height - width : 0;
		const diffX = piecesX / -2 * diffWidth;
		const diffY = piecesY / 2 * diffWidth;
		const [x, y] = rotateCoordinates({
			point: [pieceX, pieceY],
			around: [centerX, centerY],
			rotation,
		});
		const left = x * (width + diffWidth) + offsetX + diffX;
		const right = (x + 1) * (width + diffWidth) + offsetX + diffX;
		const top = y * (height - diffWidth) + offsetY + diffY;
		const bottom = (y + 1) * (height - diffWidth) + offsetY + diffY;
		return {
			left, right, top, bottom,
			width: isOnSide ? height : width,
			height: isOnSide ? width : height,
		};
	};
}

function distanceToVisibleX({ pieceLeft, pieceRight, clientWidth }) {
	if (pieceLeft < 0) { return -pieceLeft; }
	if (clientWidth < pieceRight) { return clientWidth - pieceRight; }
	return 0;
}

function distanceToVisibleY({ pieceTop, pieceBottom, clientHeight }) {
	if (pieceTop < 0) { return -pieceTop; }
	if (clientHeight < pieceBottom) { return clientHeight - pieceBottom; }
	return 0;
}

function rotateCoordinates({ point: [pX, pY], around: [aX, aY], rotation }) {
	if (rotation === 0) { return [pX, pY]; }
	if (rotation === 90) { return [aX + aY - pY, aY - aX + pX]; }
	if (rotation === 180) { return [aX + aX - pX, aY + aY - pY]; }
	if (rotation === 270) { return [aX - aY + pY, aY + aX - pX]; }
}

function matchesWith({ piece: { offsetX: xA, offsetY: yA, rotation: rotA }, canSnap }) {
	return ({ offsetX: xB, offsetY: yB, rotation: rotB }) => canSnap([xA, yA], [xB, yB]) && rotA === rotB;
}

function filterConnectedTo({ target, pieces, group = [target], rest = without([target], pieces) }) {
	const canConnectTo = group => piece => group.find(isNeighborOf(piece));
	const [members, leftovers] = partition(canConnectTo(group), rest);
	if (!members.length) { return group; }
	return filterConnectedTo({ target, group: [...group, ...members], rest: leftovers });
}

function isNeighborOf({ rotation: rA, dataset: { x, y } }) {
	const xA = parseInt(x);
	const yA = parseInt(y);
	return ({ rotation: rB, dataset }) => {
		if (rA !== rB) { return false; }
		const xB = parseInt(dataset.x);
		const yB = parseInt(dataset.y);
		return (Math.abs(xA - xB) === 1 && yA === yB) || (xA === xB && Math.abs(yA - yB) === 1);
	};
}

function findNewConnectedPiece({ connections, connectedPieces }) {
	if (!connectedPieces.length) { return false; }
	const [{ id }] = connectedPieces;
	const initialConnections = (connections[id] ?? []).map(prop('id'));
	const initialIds = initialConnections.concat(id);
	return connectedPieces.find(({ id }) => !initialIds.includes(id));
}

function arePointsWithin({ range }) {
	return ([xa, ya], [xb, yb]) => Math.abs(xa - xb) <= range && Math.abs(ya - yb) <= range;
}

export function isSolved({ canSnap = arePointsWithin({ range: 0 }), piece: { offsetX, offsetY, rotation } }) {
	return canSnap([offsetX, offsetY], [0, 0]) && rotation === 0;
}

function isSolvedBoard({ pieces }) {
	return !Array.from(pieces).find(piece => !isSolved({ piece }));
}

async function playSolvedEffect({ board, pieceDrawer }) {
	const options = { duration: 500, fill: 'both', ease: 'ease-in-out' };
	pieceDrawer.style.zIndex = 0;
	await Promise.all([
		pieceDrawer.animate({ opacity: [1, 0] }, options).finished,
		board.animate({ filter: ['brightness(1)', 'brightness(1.2)', 'brightness(1)'] }, options).finished,
	]);
}

export function solvePiece(piece) {
	Object.assign(piece, { offsetX: 0, offsetY: 0, rotation: 0 });
	piece.setAttribute('disabled', '');
}

function ascendBy(mapper) {
	return (a, b) => mapper(a) < mapper(b) ? -1 : 1;
}

function wait(msec) {
	return new Promise(resolve => setTimeout(resolve, msec));
}
