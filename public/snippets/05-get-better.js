export function setup({ playground, params }) {
	playground.style.padding = 0;
	playground.innerHTML = /* html */ `
		<link rel="stylesheet" href="snippets/05-get-better.css">
		<h1 id="content">
			Get
				<span id="wrapper">
					<span id="the-a">a</span>
				</span>
			better
			<span id="name"></span>
			!
		</h1>
	`;
	const nameElement = playground.querySelector('#name');
	const contentElement = playground.querySelector('#content');
	nameElement.textContent = params.name;
	setTimeout(() => {
		contentElement.setAttribute('data-active', '');
	}, 1000);
}
