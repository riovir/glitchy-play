const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		position: relative;
		--sprite-scale: 1;
	}
	#animation {
		background-position: top 0px left 0px;
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		border-radius: inherit;
	}
</style>
<style id="default-style"></style>
<div id="animation"></div>
<img id="image" style="display: none;">
`;

class SpriteAnimation extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._labelElement = shadowRoot.querySelector('#label');
		this._animationElement = shadowRoot.querySelector('#animation');
		this._imageElement = shadowRoot.querySelector('#image');
		this._defaultStyle = shadowRoot.querySelector('#default-style');

		this.animateSprite = this.animateSprite.bind(this);
		this.cancelAnimation = this.cancelAnimation.bind(this);
		this._updateAll = this._updateAll.bind(this);
		this._loadSprite = this._loadSprite.bind(this);
		this._updateSprite = this._updateSprite.bind(this);
	}

	connectedCallback() {
		this._updateAll();
	}

	static get observedAttributes() {
		return ['columns', 'duration', 'frames', 'src'];
	}

	attributeChangedCallback(name) {
		if (name === 'src') {
			return this._updateAll();
		}
		if (['frames', 'columns'].includes(name)) {
			return this._updateSprite();
		}
	}

	set src(value) {
		this.setAttribute('src', value);
		this._updateAll();
	}

	get src() {
		return this.getAttribute('src');
	}

	set duration(value) {
		this.setAttribute('duration', value);
	}

	get duration() {
		if (!this.hasAttribute('duration')) {
			return Math.round(this.frames / 30 * 1000);
		}
		return Math.floor(this.getAttribute('duration'));
	}

	set frames(value) {
		this.setAttribute('frames', value);
		this._updateSprite();
	}

	get frames() {
		return Math.floor(this.getAttribute('frames'));
	}

	set columns(value) {
		this.setAttribute('columns', value);
		this._updateSprite();
	}

	get columns() {
		if (!this.hasAttribute('columns')) { return this.frames; }
		return Math.floor(this.getAttribute('columns'));
	}

	animateSprite(customOptions = {}) {
		const easing = `steps(${this.frames - 1})`;
		const options = { duration: this.duration, fill: 'forwards', ...customOptions, easing };
		this._animation = this._animationElement.animate(this.keyframes, options);
		return this._animation;
	}

	cancelAnimation() {
		this._animation?.cancel();
	}

	get keyframes() {
		return frameOffsetsOf(this)
				.map(({ x, y }) => `calc(-${x} * 100%) calc(-${y}* 100%)`)
				.map(backgroundPosition => ({ backgroundPosition }));
	}

	get animation() {
		return this._animation || null;
	}

	_updateAll() {
		return this._loadSprite().then(this._updateSprite);
	}

	_loadSprite() {
		return new Promise(resolve => {
			const img = document.createElement('img');
			img.setAttribute('rel', 'preload');
			img.src = this.src;
			img.onload = () => {
				this._spriteElement = img;
				this.dispatchEvent(new Event('load-sprite'));
				resolve();
			};
		});
	}

	_updateSprite() {
		if (!this._spriteElement) { return; }
		const spriteWidth = Math.round(this._spriteElement.width / this.columns);
		const spriteHeight = Math.round(this._spriteElement.height / rowsOf(this));
		this._defaultStyle.innerHTML = /* css */`
			:host {
				width: calc(${spriteWidth}px * var(--sprite-scale));
				height: calc(${spriteHeight}px * var(--sprite-scale));
			}
		`;
		const style = this._animationElement.style;
		style.backgroundImage = `url('${this.src || ''}')`;
		style.backgroundSize = `calc(100% * ${this.columns})`;
	}
}

customElements.define('sprite-animation', SpriteAnimation);

function rowsOf({ columns, frames }) {
	const rows = Math.floor(frames / columns);
	return columns * rows < frames ? rows + 1: rows;
}

/**
 * XXX: Yeah, that's not how size calculation works.
 * At least it's correct for a single child. "To fix later".
 * */
function frameOffsetsOf({ frames, columns }) {
	let indexes = [];
	for (let frame = 0; frame < frames; frame++) {
		indexes.push({
			x: frame % columns,
			y: Math.floor(frame / columns),
		});
	}
	return indexes;
}
