export function editImage({ rotate = 0 }) {
	const angle = rotate * Math.PI / 180;
	const dimensionsOf = rotateDimensions({ angle });
	return image => {
		const { width, height } = dimensionsOf(image);

		const canvas = document.createElement('canvas');
		Object.assign(canvas, { width, height });

		const context = canvas.getContext('2d');
		context.translate(width / 2, height / 2);
		context.rotate(angle);
		context.drawImage(image, -image.width / 2, -image.height / 2);

		const srcBase64 = canvas.toDataURL();
		return { srcBase64, width, height };
	};
}

function rotateDimensions({ angle }) {
	const { abs, sin, cos } = Math;
	return ({ width, height }) => ({
		width: abs(width * cos(angle)) + abs(height * sin(angle)),
		height: abs(width * sin(angle)) + abs(height * cos(angle)),
	});
}
