
export const snippets = [
	'snippets/01-run-run-run.js',
	'snippets/02-the-cast.js',
	'snippets/03-forest.js',
	'snippets/04-puzzle-setup/index.js',
	'snippets/04-puzzle/index.js',
	'snippets/05-get-better.js',
];
