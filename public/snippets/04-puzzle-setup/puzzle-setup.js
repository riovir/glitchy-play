import '../04-puzzle/index.js';
import './pieces-input.js';

const DEFAULT_IMAGE = 'assets/crafty-bot-512x512.jpg';

export function setup({ playground, params, storage }) {
	playground.innerHTML = /* html */`
		<link rel="stylesheet" href="snippets/04-puzzle-setup/puzzle-setup.css">
		<div class="puzzle-settings-layout">
			<label id="search-image-label">Search ArtStation</label>
			<combo-box id="search-image" aria-labelledby="search-image-label" class="has-addon">
				<option>Car</option>
				<option>Motorcycle</option>
				<option>Fantasy</option>
				<option>Coffee</option>
				<option>Dog</option>
				<option>Space</option>
			</combo-box>
			<button id="search-image-button" class="button" tabindex="-1">Search</button>

			<label for="image-url">Puzzle image URL</label>
			<input id="image-url" class="input has-addon" type="url" pattern="https://.*" autocomplete="off">
			<button id="image-clear-button" class="button">Clear</button>

			<pieces-input id="pieces"></pieces-input>

			<label for="pieces-scrambled">Scramble pieces</label>
			<input id="pieces-scrambled" class="input" type="checkbox">

			<label for="pieces-rotated">Rotate pieces</label>
			<input id="pieces-rotated" class="input" type="checkbox">

			<label for="match-orientation">Match orientation</label>
			<input id="match-orientation" class="input" type="checkbox">

			<details>
				<summary>Import / export gallery</summary>
				<div class="export-layout">
					<textarea id="gallery-input" class="input is-scrollable" rows="6"></textarea>
					<button id="gallery-export-button" class="button">Export</button>
					<button id="gallery-import-button" class="button">Import</button>
					<button id="gallery-download-button" class="button">Download</button>
				</div>
			</details>

			<orientable-image id="puzzle-image" class="image"></orientable-image>

			<div id="gallery" class="is-scrollable"></div>

			<div id="buttons">
				<button id="gallery-edit-button" class="button">Edit Gallery</button>
				<button id="share-button" class="button">Share</button>
				<button id="play-button" class="button">Play</button>
			</div>
		</div>
	`;

	const gallery = playground.querySelector('#gallery');
	const galleryInput = playground.querySelector('#gallery-input');
	const galleryEditButton = playground.querySelector('#gallery-edit-button');
	const galleryExportButton = playground.querySelector('#gallery-export-button');
	const galleryImportButton = playground.querySelector('#gallery-import-button');
	const galleryDownloadButton = playground.querySelector('#gallery-download-button');
	const imageClearButton = playground.querySelector('#image-clear-button');
	const imageInput = playground.querySelector('#image-url');
	const matchOrientationInput = playground.querySelector('#match-orientation');
	const piecesRotatedInput = playground.querySelector('#pieces-rotated');
	const piecesScrambledInput = playground.querySelector('#pieces-scrambled');
	const playButton = playground.querySelector('#play-button');
	const piecesInput = playground.querySelector('#pieces');
	const puzzleImage = playground.querySelector('#puzzle-image');
	const searchUnsplashInput = playground.querySelector('#search-image');
	const searchUnsplashButton = playground.querySelector('#search-image-button');
	const shareButton = playground.querySelector('#share-button');

	const updateImage = async () => {
		const src = params.src || 'assets/crafty-bot-512x512.jpg';
		puzzleImage.src = src;
		await puzzleImage.updated;
	};
	updateImage();

	searchUnsplashInput.addEventListener('submit', openUnsplash);
	searchUnsplashButton.addEventListener('click', () => searchUnsplashInput.submit());

	imageInput.value = params.src || '';
	imageInput.addEventListener('input', ({ target }) => {
		params.src = target.value;
		updateImage();
	});
	imageClearButton.addEventListener('click', () => {
		update({ input: imageInput, value: '' });
	});

	piecesScrambledInput.checked = params.scramble !== 'false';
	params.scramble = piecesScrambledInput.checked;
	piecesScrambledInput.addEventListener('change', ({ target }) => {
		params.scramble = target.checked;
	});

	piecesRotatedInput.checked = params.rotate !== 'false';
	params.rotate = piecesRotatedInput.checked;
	piecesRotatedInput.addEventListener('change', ({ target }) => {
		params.rotate = target.checked;
	});

	const updateOrientation = () => {
		storage.matchOrientation = matchOrientationInput.checked;
		const getOrientation = () => playground.clientWidth < playground.clientHeight ? 'portrait' : 'landscape';
		const orientation = !matchOrientationInput.checked ? null : getOrientation();
		params.orientPuzzle = orientation;
		puzzleImage.orientation = orientation;
	};
	matchOrientationInput.checked = !!storage.matchOrientation;
	matchOrientationInput.addEventListener('change', updateOrientation);
	updateOrientation();

	puzzleImage.addEventListener('load', rememberImage({ storage }));

	galleryExportButton.addEventListener('click', () => {
		galleryInput.value = storage.puzzleImages.join('\n');
		galleryInput.select();
	});
	galleryImportButton.addEventListener('click', async () => {
		const knownUrls = storage.puzzleImages;
		const newEntries = await Promise.all(galleryInput.value.split(/\s/gm)
				.map(line => line.trim())
				.filter(Boolean)
				.filter(url => !knownUrls.includes(url))
				.map(tryLoading));
		const newUrls = newEntries.filter(Boolean);
		galleryInput.value = '';
		if (!newUrls.length) { return; }
		storage.puzzleImages = [...newUrls, ...knownUrls];
		location.reload();
	});

	galleryDownloadButton.addEventListener('click', () => {
		const anchor = document.createElement('a');
		anchor.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(storage.puzzleImages.join('\n'))}`);
		anchor.setAttribute('download', 'puzzle-gallery.txt');

		anchor.style.display = 'none';
		document.body.appendChild(anchor);

		anchor.click();

		document.body.removeChild(anchor);
	});

	galleryEditButton.addEventListener('click', () => playground.toggleAttribute('data-gallery-edit-active'));

	if (!navigator.share) { shareButton.style.display = 'none'; }
	shareButton.addEventListener('click', () => {
		navigator.share({
			title: 'Glitchy Play',
			text: `A ${params.piecesX}x${params.piecesY} puzzle`,
			url: location.href.replace('04-puzzle-setup.js', '04-puzzle/index.js'),
		});
	});

	playButton.addEventListener('click', async () => {
		const { piecesX, piecesY } = piecesInput;
		Object.assign(params, { piecesX, piecesY });
		params.seed = params.seed ?? String(Math.random()).slice(2);
		await new Promise(resolve => setTimeout(resolve, 100));
		location.hash = location.hash.replace('04-puzzle-setup/index.js', '04-puzzle/index.js');
	});

	(storage.puzzleImages ?? []).forEach(src => {
		const { button, removeButton } = ImageButton({ src });
		button.addEventListener('click', () => {
			update({ input: imageInput, value: src });
		});
		removeButton.addEventListener('click', event => {
			event.stopPropagation();
			forgetImage({ src, storage });
			update({ input: imageInput, value: '' });
			button.remove();
		});
		gallery.appendChild(button);
	});

	piecesInput.setBoardElement(playground);
	window.addEventListener('resize', () => piecesInput.setBoardElement(playground));
	Object.assign(piecesInput, {
		pieceMinSizePx: storage.pieceMinSizePx ?? 48,
		piecesPreferred: storage.piecesPreferred ?? 10,
	})
	piecesInput.addEventListener('pieces-changed', ({ target: { pieceMinSizePx, piecesPreferred } }) => {
		Object.assign(storage, { pieceMinSizePx, piecesPreferred });
	});
	puzzleImage.addEventListener('load', ({ target }) => piecesInput.setPuzzleImage(target));
}

function openUnsplash({ target: { value } }) {
	if (!value) { return; }
	const search = encodeURIComponent(value.toLowerCase().trim());
	open(`https://www.artstation.com/search?query=${search}`, '_blank').focus();
}

export function rememberImage({ storage }) {
	return ({ target: { src } }) => {
		if (typeof src !== 'string') { return; }
		const images = storage.puzzleImages ?? [];
		const isNotDefault = src => !src.endsWith(DEFAULT_IMAGE);
		storage.puzzleImages = [... new Set([src, ...images])]
				.filter(isNotDefault);
	};
}

function forgetImage({ storage, src }) {
	storage.puzzleImages = storage.puzzleImages.filter(value => value !== src);
}

function ImageButton({ src = '', alt = '' } = {}) {
	const button = document.createElement('button');
	button.className = 'button is-image';
	const image = document.createElement('img');
	image.className = 'image';
	button.appendChild(image);

	const removeButton = document.createElement('button');
	removeButton.classList = 'button is-edit-action';
	removeButton.setAttribute('aria-label', 'Remove image')
	removeButton.innerHTML = /* html */`
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width="100%" height="100%" focusable="false" aria-hidden="true">
			<path fill="currentColor" d="M 0 1 l 1 -1 l 4 4 l 4 -4 l 1 1 l -4 4 l 4 4 l -1 1 l -4 -4 l -4 4 l -1 -1 l 4 -4 z" />
		</svg>
	`;
	image.addEventListener('load', () => {
		const { naturalWidth, naturalHeight } = image;
		button.style.aspectRatio = `${naturalWidth} / ${naturalHeight}`;
	});
	button.appendChild(removeButton);

	Object.assign(image, { src, alt });
	return { button, image, removeButton };
}

function update({ input, value }) {
	if (input.value === value) { return; }
	input.value = value;
	input.dispatchEvent(new Event('input'));
}

export function tryLoading(src) {
	return new Promise(resolve => {
		const img = document.createElement('img');
		img.onload = () => resolve(src);
		img.onerror = () => resolve(null);
		img.setAttribute('rel', 'preload');
		img.src = src;
	});
}
