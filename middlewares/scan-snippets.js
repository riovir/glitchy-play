import { generateSnippetsIndex } from '../build/generate-snippets-index.js';

export async function scanSnippets(ctx, next) {
	if (ctx.method !== "GET" || ctx.url !== '/generated/snippets-index.js') {
		return next();
	}
	const content = await generateSnippetsIndex();
	ctx.set('content-type', 'application/javascript; charset=utf-8');
	ctx.body = content;
}
