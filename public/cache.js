const PRODUCTION_MODE = true; // Altered by the dev-server
const VERSION = 1;
const CACHE_NAME = PRODUCTION_MODE ? `my-cache-v${VERSION}` : `dev-cache-v${VERSION}`;

self.addEventListener('install', () => { self.skipWaiting(); });
self.addEventListener('fetch', respondWith(PRODUCTION_MODE ?
	staleWhileRevalidate :
	cacheFirst));
self.addEventListener('activate', reloadAllTabs);
self.addEventListener('activate', waitUntil(deleteStaleCaches));

async function staleWhileRevalidate(event) {
	const { request } = event;

	if (!isSameOrigin(request)) {
		return cacheFirst(event);
	}
	const cache = await caches.open(CACHE_NAME);

	const cachedResponse = await cache.match(request);
	const networkResponsePromise = fetch(request).catch(() => null);

	if (request.method === 'GET') {
		event.waitUntil(update({ cache, request, response: networkResponsePromise }));
	}

	return cachedResponse || networkResponsePromise;
}

async function cacheFirst(event) {
	const cache = await caches.open(CACHE_NAME);
	const { request } = event;
	const cachedResponse = await cache.match(request);
	return cachedResponse || fetch(request).catch(() => null);
}

async function update({ cache, request, response }) {
	const _response = await response;
	if (!_response || isPartial(_response)) { return; }
	await cache.put(request, _response.clone());
}

function isPartial({ status }) {
	return status === 206;
}

function isSameOrigin({ url }) {
	try {
		const { origin } = new URL(url);
		return origin === self.location.origin;
	}
	catch(err) {
		console.error('Invalid URL', err, url);
		return false;
	}
}

async function deleteStaleCaches() {
	const cacheNames = await caches.keys();
	await Promise.all(cacheNames
		.filter(cacheName => cacheName !== CACHE_NAME)
		.map(cacheName => caches.delete(cacheName)));
}

async function reloadAllTabs() {
	const tabs = await self.clients.matchAll({ type: 'window' });
	tabs.forEach(tab => { tab.navigate(tab.url); });
}

function waitUntil(fn) {
	return event => event.waitUntil(fn(event));
}

function respondWith(fn) {
	return event => event.respondWith(fn(event));
}
