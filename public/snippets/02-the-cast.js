export function setup({ playground }) {
	playground.innerHTML = /* html */ `
		<link rel="stylesheet" href="snippets/02-the-cast.css">
	`;

	renderSprite({ playground, frames: 16, columns: 16, asset: 'meal-vendor-walking.png' });
	renderSprite({ playground, frames: 24, columns: 8, asset: 'adventure-piggy-walking.png' });
	renderSprite({ playground, frames: 28, columns: 6, asset: 'fox-jump.png' });
	renderSprite({ playground, frames: 95, columns: 5, asset: 'forehorseman-idle.png' });
}

function renderSprite({
	asset,
	src = `assets/${asset}`,
	frames,
	columns = frames,
	playground
}) {
	const sprite = document.createElement('sprite-animation');
	Object.assign(sprite, { src, frames, columns });

	const trigger = document.createElement('animation-trigger');
	trigger.appendChild(sprite);

	playground.appendChild(trigger);
}
