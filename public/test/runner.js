import { setup, reportResults } from './tests.js';
import { View } from './view.js';

const view = View({ container: document.querySelector('#results'), console });
const { loadTests, runTests } = setup({
	basePath: '../snippets/',
	globals: window,
});

const specs = [
	'04-puzzle/jigsaw-cutout.spec.js',
	'04-puzzle/use-update.spec.js',
	'04-puzzle-setup/pieces-input.spec.js',
];

loadTests(specs)
	.then(runTests)
	.then(reportResults(view));
