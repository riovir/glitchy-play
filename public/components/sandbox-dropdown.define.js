if (!("anchorName" in document.documentElement.style)) {
	const { default: polyfill } = await import("https://unpkg.com/@oddbird/css-anchor-positioning/dist/css-anchor-positioning-fn.js");
	polyfill(true);
}

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		display: inline-block;
	}
	#invoker {
		anchor-name: --anchor-invoker;
		border-radius: 0.25em;
		padding: 0.25em 0.5em;
		box-shadow: inset 0px 0.125em 0.5em rgba(51, 51, 51, 0.3);
	}
	#content {
		bottom: anchor(--anchor-invoker top);
		left: anchor(--anchor-invoker center);
	}
</style>
<button id="invoker" aria-expanded="false" popovertarget="content">
	<slot name="invoker">
		<span id="caption"></span>
	</slot>
</button>
<div id="content" popover>
	<slot></slot>
</div>
`;

class SandboxDropdown extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));
		this._captionNode = shadowRoot.querySelector('#caption');
		this._contentNode = shadowRoot.querySelector('#content');
		this._invokerNode = shadowRoot.querySelector('#invoker');
	}

	connectedCallback() {
		this.caption = this._caption;
	}

	set caption(value) {
		this._captionNode.textContent = String(value ?? '');
	}

	get caption() {
		return this._captionNode.textContent;
	}

	set opened(value) {
		this._invokerNode.setAttribute('opened', value ? 'true' : 'false');
	}

	get opened() {
		return this._invokerNode.getAttribute('opened') === 'true';
	}
}

customElements.define('sandbox-dropdown', SandboxDropdown);
