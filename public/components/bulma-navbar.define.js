const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		box-sizing: border-box;
		display: block;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		height: var(--spacing-navbar-height);
		color: var(--color-fg-default);
		background: var(--color-bg-subtle);
		z-index: var(--z-topmost, 999);
	}
	:host * {
		box-sizing: inherit;
	}

	.navbar {
		display: grid;
		position: relative;
		height: 100%;
	}

	.navbar-brand {
		display: flex;
		gap: var(--spacing-2);
		align-items: center;
		padding-left: var(--spacing-4);
		padding-right: var(--spacing-3);
	}

	.navbar-menu {
		display: none;
		position: absolute;
		top: var(--spacing-navbar-height);
		width: 100%;
		padding-top: var(--spacing-3);
		padding-bottom: var(--spacing-3);
		background: var(--color-bg-default);
	}
	.navbar-menu.is-active {
		display: grid;
	}
	.navbar-menu ::slotted(*) {
		width: 100%;
		padding: var(--spacing-1) var(--spacing-2);
		line-height: calc(var(--spacing-navbar-height) - 2 * var(--spacing-1));
		color: currentColor;
		text-decoration: none;
		text-transform: capitalize;
		white-space: nowrap;
	}
	.navbar-menu ::slotted(:hover) {
		color: var(--color-fg-primary-default);
		background: var(--color-bg-primary-default);
	}
	.navbar-menu ::slotted(.is-active) {
		color: var(--color-fg-primary-emphasis);
		background: var(--color-bg-primary-soft);
		cursor: default;
	}

	.navbar-start,
	.navbar-end {
		display: flex;
		flex-direction: column;
	}

	.navbar-burger {
		height: 100%;
		margin-left: auto;
		width: var(--spacing-7);
		position: relative;
		color: currentColor;
		border: none;
		background: none;
		cursor: pointer;
	}

	@media (hover: hover) {
		.navbar-burger:hover {
			color: var(--color-fg-primary-default);
			background: var(--color-bg-primary-default);
		}
	}

	.navbar-burger span {
		background-color: currentColor;
		display: block;
		height: 1px;
		left: calc(50% - var(--spacing-2));
		position: absolute;
		transform-origin: center;
		transition-duration: var(--speed-1);
		transition-property: background-color, opacity, transform;
		transition-timing-function: ease-out;
		width: 1rem;
	}

	.navbar-burger span:nth-child(1) {
		top: calc(50% - 6px);
	}

	.navbar-burger span:nth-child(2) {
		top: calc(50% - 1px);
	}

	.navbar-burger span:nth-child(3) {
		top: calc(50% + 4px);
	}

	.navbar-burger.is-active span:nth-child(1) {
		transform: translateY(5px) rotate(45deg);
	}

	.navbar-burger.is-active span:nth-child(2) {
		opacity: 0;
	}

	.navbar-burger.is-active span:nth-child(3) {
		transform: translateY(-5px) rotate(-45deg);
	}

	.is-scrollable-x {
		overflow-x: auto;
		max-width: calc(100vw - 16ch);
		scrollbar-width: thin;
	}
	.is-scrollable-x::-webkit-scrollbar {
		width: 8px;
		height: 8px;
		background-color: var(--color-scrollbar-bg);
	}
	.is-scrollable-x::-webkit-scrollbar-thumb {
		background: var(--color-scrollbar-thumb);
	}

	@media screen and (min-width: 1024px) {
		:host {
			background: var(--color-bg-subtle);
			box-shadow: var(--shadow-1);
		}

		.navbar {
			grid-template-columns: auto 1fr;
		}

		.navbar-burger {
			display: none;
		}

		.navbar-menu {
			display: grid;
			position: static;
			grid-template-columns: 1fr auto;
			padding: 0;
			gap: var(--spacing-2);
			background: transparent;
		}

		.navbar-start,
		.navbar-end {
			flex-direction: row;
		}
	}
</style>
<nav class="navbar" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<slot name="brand"></slot>
		<button class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="the-menu">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</button>
	</div>
	<div id="the-menu" class="navbar-menu">
		<div class="navbar-start is-scrollable-x">
			<slot></slot>
		</div>

		<div class="navbar-end">
			<slot name="end"></slot>
		</div>
	</div>
</nav>
`;

export class BulmaNavbar extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._menuElement = shadowRoot.querySelector('#the-menu');
		this._burgerElement = shadowRoot.querySelector('[data-target="the-menu"]');
		this._burgerElement.addEventListener('click', () => this.active = !this.active);
		window.addEventListener('click', ({ target }) => {
			if (isOutsideOf({ parent: this, target })) {
				this.active = false;
			}
		});
	}

	get active() {
		return this._burgerElement.getAttribute('aria-expanded') === 'true';
	}
	set active(value) {
		if (value === this.active) { return; }
		if (value) { this.setAttribute('active', ''); }
		else { this.removeAttribute('active'); }

		this._burgerElement.setAttribute('aria-expanded', !!value);
		this._menuElement.classList.toggle('is-active');
		this._burgerElement.classList.toggle('is-active');
	}
}

customElements.define('bulma-navbar', BulmaNavbar);

function isOutsideOf({ parent, target }) {
	if (!target) {
		return true;
	}
	if (parent === target) {
		return false;
	}
	return isOutsideOf({ parent, target: target.parentElement });
}
