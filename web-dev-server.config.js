import { scanSnippets, serveDevSw } from './middlewares/index.js';

export default {
	open: true,
	rootDir: './public',
	middleware: [scanSnippets, serveDevSw],
};
