export function play({ playground }) {
	/* Reset Shroom Guy */
	document.querySelector('#shroom-guy')?.remove();
	setup({ playground });
	const shroomGuy = document.querySelector('#shroom-guy');
	const piggy = document.querySelector('#piggy');
	const swoosh = document.querySelector('#swoosh');

	const animate = (element, { direction } = {}) => {
		/* Teach him to run */
		const isLeft = direction === 'left';
		const transform = isLeft ?
				[
					'translateX(0) scaleX(-1)',
					`translateX(-${playground.clientWidth}px) scaleX(-1)`
				] :
				[
					'translateX(0)',
					`translateX(${playground.clientWidth}px)`
				];
		const run = options => element.animate(
			{ transform },
			{ duration: 200, easing: 'ease-in', fill: 'both', ...options });

		/* Teach him to walk */
		const walk = ({ steps }) => {
			const options = { iterations: steps, duration: 2000 / steps };
			return element.animateSprite(options).finished;
		}

		/* Look at him go! */
		return walk({ steps: 3 })
		.then(() => walk({ steps: 5 }))
		.then(() => walk({ steps: 9 }))
		.then(() => Promise.all([
			swoosh.play(),
			walk({ steps: 20 }),
			run({ delay: 450 }),
		]));
	};

	return Promise.all([
		animate(piggy, { direction: 'left' }),
		delay({ msec: 500 }).then(() => animate(shroomGuy)),
	]);
}

export function setup({ playground }) {
	playground.innerHTML = /* html */ `
		<link rel="stylesheet" href="snippets/01-run-run-run.css">
		<!--
			Did you know that the creators of Slack also a game called
			Glitch in 2009? They made all their assets public domain in 2012.
			Respect.
			https://www.glitchthegame.com/inhabitants/vendors/meal-vendor/
		-->
		<sprite-animation
			id="shroom-guy"
			src="assets/meal-vendor-walking.png"
			frames="16"
		></sprite-animation>
		<sprite-animation
			id="piggy"
			src="assets/adventure-piggy-walking.png"
			frames="24"
			columns="8"
		></sprite-animation>
		<!-- Sound from Zapsplat.com -->
		<audio id="swoosh" src="assets/swipe.mp3" preload="auto"></audio>
	`;
}

function delay({ msec }) {
	return new Promise(resolve => setTimeout(resolve, msec));
}
