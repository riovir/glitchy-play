const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		box-sizing: border-box;
		width: 100%;
		max-width: 100%;
		height: 100%;
		max-height: 100%;
		overflow: auto;
		scrollbar-width: thin;
	}
	:host * {
		box-sizing: inherit;
	}

	:host::-webkit-scrollbar {
		width: var(--spacing-scrollbar-width);
		height: var(--spacing-scrollbar-width);
		background-color: var(--color-scrollbar-bg);
	}
	:host::-webkit-scrollbar-thumb {
		background: var(--color-scrollbar-thumb);
	}
	:host::-webkit-scrollbar-corner {
		background: var(--color-scrollbar-bg);
	}

	#wrapper {
		overflow: hidden;
		height: 100%;
		width: 100%;
	}

	#content {
		transform-origin: top left;
	}
</style>
<div id="wrapper">
	<div id="content">
		<slot></slot>
	</div>
</div>
`;

class ResponsiveCanvas extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._contentWidth = null;
		this._contentHeight = null;

		this._wrapperElement = shadowRoot.querySelector('#wrapper');
		this._contentElement = shadowRoot.querySelector('#content');
		this._rescaleContent = this._rescaleContent.bind(this);
		this.reset = this.reset.bind(this);
	}

	reset() {
		this._contentWidth = null;
		this._contentHeight = null;
		this._wrapperElement.removeAttribute('style');
		this._contentElement.removeAttribute('style');
	}

	connectedCallback() {
		this._rescaleInterval = setInterval(this._rescaleContent, 250);
	}
	disconnectedCallback() {
		clearInterval(this._rescaleInterval);
	}

	_rescaleContent() {
		const { width: _contentWidth, height: _contentHeight } = contentRect(this);
		if (this._contentHeight === _contentHeight && this._contentWidth === _contentWidth) { return; }
		Object.assign(this, { _contentHeight, _contentWidth });

		const { width: selfWidth, height: selfHeight } = this.getBoundingClientRect();
		const ratioX = Math.min(selfWidth / _contentWidth, 1);
		const ratioY = Math.min(selfHeight / _contentHeight, 1);
		const ratio = Math.max(ratioX, ratioY);
		const contentWidthScaled = _contentWidth * ratio;
		const contentHeightScaled = _contentHeight * ratio;
		const scrollbarX = selfHeight < _contentHeight ? 'var(--spacing-scrollbar-width)' : '0px';
		const scrollbarY = selfWidth < _contentWidth ? 'var(--spacing-scrollbar-width)' : '0px';
		this._wrapperElement.style.width = `calc(${contentWidthScaled}px - ${scrollbarX})`;
		this._wrapperElement.style.height = `calc(${contentHeightScaled}px - ${scrollbarY})`;
		if (1 <= ratio) { return; }
		this._contentElement.style.transform = `scale(${ratio})`;
		this.scrollTo(0, 9999);
	}
}

function contentRect({ children }) {
	let width = 0;
	let height = 0;
	for(let element of children) {
		const rect = element.getBoundingClientRect();
		width += rect.width;
		height += rect.height;
	}
	return { width, height };
}

customElements.define('responsive-canvas', ResponsiveCanvas);
