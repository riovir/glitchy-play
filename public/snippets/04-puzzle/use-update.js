export function useUpdate({ update }) {
	const changedProps = new Map();
	let updating = false;

	let _complete = Promise.resolve(true);
	const complete = { get value() { return _complete; } };

	async function requestUpdate(prop, oldValue) {
		if (prop) {
			changedProps.set(prop, oldValue);
		}
		if (updating) {
			return _complete;
		}
		updating = true;
		_complete = new Promise(async resolve => {
			await _complete;
			updating = false;
			await update(changedProps);
			changedProps.clear();
			resolve(true);
		});
		return _complete;
	}

	return { complete, requestUpdate };
}
