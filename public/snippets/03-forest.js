export function setup({ playground }) {
	playground.style.padding = 0;
	playground.innerHTML = /* html */ `
		<link rel="stylesheet" href="snippets/03-forest.css">
		<responsive-canvas>
			<div id="content"></div>
		</responsive-canvas>
	`;
	const content = playground.querySelector('#content');

	renderSprite({ content, id: 'batterfly', frames: 40, columns: 8, asset: 'batterfly-side-turned.png' });
	renderSprite({ content, id: 'butterfly', frames: 84, columns: 14, asset: 'butterfly-fly.png' });
	renderSprite({ content, id: 'cat', animations: [
		Sprite({ frames: 47, columns: 7, asset: 'cat-appear.png' }),
		Sprite({ frames: 38, columns: 7, asset: 'cat-happy.png' }),
		Sprite({ frames: 28, columns: 7, asset: 'cat-disappear.png' }),
	] });
	renderSprite({ content, id: 'crab', frames: 24, columns: 6, asset: 'crab-like.png', iterations: 4 });
	renderSprite({ content, id: 'lizard', animations: [
		Sprite({ frames: 25, columns: 3, asset: 'lizard-appear.png' }),
		Sprite({ frames: 33, columns: 3, asset: 'lizard-disappear.png' }),
	] });
	renderSprite({ content, id: 'fox', frames: 28, columns: 6, asset: 'fox-jump.png' });
	renderSprite({ content, id: 'forehorseman', frames: 95, columns: 5, asset: 'forehorseman-idle.png' });
	renderSprite({ content, id: 'stoneman', frames: 90, columns: 15, asset: 'stoneman-talk.png' });
}

function renderSprite({
	id,
	asset,
	src = `assets/${asset}`,
	frames,
	columns = frames,
	iterations = 1,
	animations = [Sprite({ asset, src, frames, columns })],
	content
}) {
	const trigger = document.createElement('animation-trigger');
	Object.assign(trigger, { iterations });
	trigger.setAttribute('id', id);

	const SpriteElement = props => {
		const sprite = document.createElement('sprite-animation');
		sprite.style.display = 'none';
		return Object.assign(sprite, props);
	}

	const sprites = animations.map(SpriteElement);
	sprites.forEach(appendTo(trigger));

	showIndex(0, sprites);
	if (1 < sprites.length) {
		trigger.addEventListener('animation-finished', () => showNextIn(sprites));
	}

	content.appendChild(trigger);
	return { trigger, sprites };
}

function Sprite({
	asset,
	src = `assets/${asset}`,
	frames,
	columns = frames,
}) {
	return { src, frames, columns };
}

function appendTo(parent) {
	return element => parent.appendChild(element);
}

function showIndex(index, sprites) {
	sprites.forEach(el => {
		el.style.display = 'none';
		el.cancelAnimation();
	});
	sprites[index].style.display = null;
}

function showNextIn(sprites) {
	const index = sprites.findIndex(({ style }) => style.display !== 'none');
	const nextIndex = index + 1 < sprites.length ? index + 1 : 0;
	showIndex(nextIndex, sprites);
}
