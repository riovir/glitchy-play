import './orientable-image.js';

const template = document.createElement('template');
template.innerHTML = /* html */`
<style>
	:host {
		display: block;
		width: 100%;
		height: 100%;
	}
	#image {
		width: 100%;
		height: 100%;
	}
</style>
<style id="cutout"></style>
<orientable-image id="image"></orientable-image>
`;

class PuzzlePiece extends HTMLElement {
	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._cutoutStyleElement = shadowRoot.querySelector('#cutout');
		this._imageElement = shadowRoot.querySelector('#image');
		this._applyCutout = this._applyCutout.bind(this);
	}

	connectedCallback() {
		this._applyCutout();
	}

	get cutoutId() {
		return this.getAttribute('cutout-id');
	}
	set cutoutId(value) {
		this.setAttribute('cutout-id', value);
		this._applyCutout();
	}

	get src() {
		return this._imageElement.src;
	}
	set src(value) {
		this._imageElement.src = value;
		this._applyCutout();
	}

	get orientation() {
		return this._imageElement.orientation;
	}
	set orientation(value) {
		this._imageElement.orientation = value;
	}

	_applyCutout() {
		if (!this.src) { return; }
		this._cutoutStyleElement.innerHTML = `
			:host {
				clip-path: url('#${this.cutoutId}');
			}
		`;
	}
}

customElements.define('puzzle-piece', PuzzlePiece);
