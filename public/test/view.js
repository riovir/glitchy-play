export function View({ container, console }) {
	const render = el => container.appendChild(el);
	const wrap = wrapWith(console);
	return wrap({
		describe: name => render(el('h2', { className: 'has-text-weight-bold' }, name)),
		pass: spec => render(el('div', { className: 'has-text-success pl-2' }, `✔ ${spec}`)),
		fail: (spec, err) => render(el('div', { className: 'has-text-danger pl-2' }, [
			() => el('div', `(ノಠ益ಠ)ノ彡 ${spec}`),
			err ? () => el('div', { className: 'pl-2' }, err.message) : null,
		])),
	});
}

function el(tag, propsOrContent, content) {
	if (content === undefined) {
		return el(tag, {}, propsOrContent);
	}
	const element = document.createElement(tag);
	Object.assign(element, propsOrContent);

	const _content = typeof content === 'function' ? content() : content;

	if (typeof _content === 'string') {
		element.textContent = content;
	}
	if (Array.isArray(_content)) {
		_content.filter(Boolean).map(fn => fn()).forEach(child => element.appendChild(child));
	}
	return element;
}

function wrapWith(console) {
	return ({ describe, pass, fail }) => ({
		describe: (...args) => {
			console.log(...args);
			return describe(...args);
		},
		pass: (...args) => {
			console.log(' ✔', ...args);
			return pass(...args);
		},
		fail: (...args) => {
			console.error(' (ノಠ益ಠ)ノ彡', ...args);
			return fail(...args);
		},
	});
}
