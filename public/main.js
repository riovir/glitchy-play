import './components/define.js';
import { snippets } from './generated/snippets-index.js';
import './cache.register.js'; // @disabled by middlewares/serve-dev-sw.js

const DEFAULT_SNIPPET = 'snippets/04-puzzle-setup/index.js';

const state = {
	snippetModule: null,
};

const context = SnippetContext();
window.__debug = { context };

const Elements = {
	navbar: document.querySelector('#navbar'),
	play: document.querySelector('#play'),
	playground: document.querySelector('#playground'),
	attribution: document.querySelector('#attribution'),
	goFullscreen: document.querySelector('#go-fullscreen'),
};

snippets.map(NavbarItem)
	.forEach(element => Elements.navbar.appendChild(element));
Elements.navbar.appendChild(Elements.attribution.content.cloneNode(true));

Elements.play.addEventListener('click', playCurrentSnippet);
Elements.goFullscreen.addEventListener('click', requestFullscreen);
if (!document.body.requestFullscreen && !document.body.webkitRequestFullscreen) { Elements.goFullscreen.style.display = 'none'; }

const snippetUrl = routeOf(location.href) || DEFAULT_SNIPPET;
if (!location.hash) { location.hash = `#${snippetUrl}`; }

loadSnippet({ snippetUrl, context });
window.addEventListener('hashchange', () => loadSnippet({ context }), false);
preloadSnippets();

function loadSnippet({ newURL = window.location.href, snippetUrl = routeOf(newURL), context }) {
	if (screen.orientation) { screen.orientation.onchange = null; }
	Elements.navbar.active = false
	return import(`./${snippetUrl}`)
		.then(({ title = snippetUrl, setup, play }) => {
			state.snippetModule = { url: snippetUrl, title, setup, play };
			return state.snippetModule;
		})
		.then(renderSnippet)
		.then(setupCurrentSnippet({ context }));
}

async function preloadSnippets() {
	return Promise.all(snippets.map(url => import(`./${url}`)));
}

function renderSnippet({ url, title = url, play }) {
	Elements.navbar = resetNavbar(Elements);
	Elements.playground = resetPlayground(Elements);

	document.title = title;
	Elements.play.style.display = play ? null : 'none';
	Elements.navbar.querySelector(`a.is-active`)?.classList.remove('is-active');
	const [routeUrl] = location.hash.split('?');
	Elements.navbar.querySelector(`a[href="${routeUrl}"]`)?.classList.add('is-active');
}

function setupCurrentSnippet({ context }) {
	return () => {
		const { navbar, playground } = Elements;
		state.snippetModule?.setup?.({ navbar, playground, ...context });
	}
};

function playCurrentSnippet() {
	const playground = Elements.playground;
	state.snippetModule?.play?.({ playground, ...context });
};

function resetNavbar({ navbar }) {
	const brandSlotted = navbar.querySelectorAll('[slot=brand]');
	for (let index = 2; index < brandSlotted.length; index++) {
		brandSlotted[index].remove();
	}
	return navbar;
}

function resetPlayground({ playground }) {
	const element = document.createElement('div');
	element.id = 'playground';
	playground.replaceWith(element);
	return element;
}

function NavbarItem(href) {
	const item = document.createElement('a')
	item.setAttribute('href', `#${href}`);
	item.setAttribute('slot', 'end');
	item.textContent = format(href);
	return item;
}

function format(url) {
	const isNotIndex = name => name !== 'index.js';
	const [file] = url.split('/').filter(isNotIndex).reverse();
	const [/* number */, ...words] = file.split('-');
	return words.join(' ').replace(/\.js$/, '');
}

function routeOf(href) {
	const { hash } = new URL(href);
	const [url] = hash.replace('#', '').split('?');
	return url;
}

function SnippetContext() {
	const params = QueryParams();
	const storage = LocalStorage();
	return { storage, params };
}

function QueryParams() {
	const getLocation = () => {
		const [snippet, query = ''] = location.hash.split('?');
		const params = new URLSearchParams(query);
		return { snippet, params };
	};
	let cache = {};
	let pushStateId = null;
	window.addEventListener('hashchange', () => { cache = {} }, false);
	return new Proxy(getLocation, {
		get: (target, prop) => {
			if (!(prop in cache)) { return target().params.get(prop); }
			return cache[prop] ?? undefined;
		},
		set: (target, prop, value) => {
			cache[prop] = value;

			if (pushStateId) clearTimeout(pushStateId);
			pushStateId = setTimeout(() => {
				const { snippet, params } = target();
				const initial = params.toString();
				Object.entries(cache).forEach(([prop, value]) => {
					params.set(prop, value);
					if (value === null || value === undefined) {
						params.delete(prop);
					}
				});
				const updated = params.toString();
				if (initial === updated) { return; }
				history.pushState(null, '', `${snippet}?${updated}`);
			}, 100);
			return true;
		},
	});
}

export function LocalStorage() {
	return new Proxy(localStorage, {
		get: (target, prop) => {
			try { return JSON.parse(target.getItem(prop)); }
			catch { return; }
		},
		set: (target, prop, value) => {
			target.setItem(prop, JSON.stringify(value));
			return true;
		},
	});
}

async function requestFullscreen() {
	if (document.body.requestFullscreen) {
		return document.body.requestFullscreen();
	}
	if (document.body.webkitRequestFullscreen) {s
		return document.body.webkitRequestFullscreen();
	}
}

const updateVh = () => {
	const vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
};
updateVh();
window.addEventListener('resize', updateVh);
